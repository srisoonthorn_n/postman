async function loadmovie() {
    let response = await fetch('https://dv-excercise-backend.appspot.com/movies')
    let data1 = await response.json()
    var resultElement = document.getElementById('ResultMovieList')
    return data1
}

function createResultTable(data) {
    let resultElement = document.getElementById('ResultMovieList')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    tableNode.setAttribute('style', 'text-align: center')

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    data.then((json) => {
        for (let i = 0; i < json.length; i++) {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)

            columnNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['imageUrl'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['name']
            dataRow.appendChild(columnNode)


            columnNode = document.createElement('td')
            columnNode1 = document.createElement('p')
            columnNode2 = document.createElement('b')
            columnNode1.innerText = currentData['synopsis']

            dataRow.appendChild(columnNode)
            columnNode.appendChild(columnNode1)
            columnNode1.appendChild(columnNode2)


        }
    })
}
async function searchMoive() {
    let nameMoive = document.getElementById('queryId').value
    if (nameMoive != '' && nameMoive != null) {
        let response = await fetch('https://dv-excercise-backend.appspot.com/movies/' + nameMoive)
        let data = await response.json()
        return data

    }

}
function createResultForm(data) {

    let resultElementForm = document.getElementById('resultSearch')

    var checkwrapper = document.getElementById('wrapper')


    var wrapper = document.createElement('div')
    wrapper.setAttribute('id', 'wrapper')
    resultElementForm.appendChild(wrapper)

    if (checkwrapper != null) {
        resultElementForm.removeChild(checkwrapper)

    }

    data.then((json) => {


        for (var i = 0; i < json.length; i++) {

            var currentData = json[i]



            let title = document.createElement('h1')
            title.innerHTML = "Movie"

            wrapper.appendChild(title)

            var imageform = document.createElement('img')
            imageform.style.width = '350px'
            imageform.style.height = '450px'
            imageform.setAttribute('src', currentData['imageUrl'])


            let moviename = document.createElement('p')
            moviename.innerHTML = "Moive Name : " + currentData['name']


            let synopsis = document.createElement('p')
            synopsis.innerHTML = "Synopsis: " + currentData['synopsis']


            wrapper.appendChild(imageform)
            wrapper.appendChild(moviename)
            wrapper.appendChild(synopsis)


        }

    })
}
