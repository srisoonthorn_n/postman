function loadAllStudents() {
    var responseData = null;
    var data = fetch('https://dv-student-backend-2019.appspot.com/students')
        .then((response) => {
            console.log(response)
            return response.json()
        })
        .then((json) => {
            responseData = json
            var resultElement = document.getElementById('result')
            resultElement.innerHTML = JSON.stringify(json, null, 2)
        })

}
async function loadAllStudentsAsync() {
    let response = await fetch('https://dv-student-backend-2019.appspot.com/students')
    let data = await response.json()
    var resultElement = document.getElementById('result')
    resultElement.innerHTML = JSON.stringify(data, null, 2)
    return data
}
function createResultTable(data) {
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'studentId'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'surname'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'gpa'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) => {
        for (let i = 0; i < json.length; i++) {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFirstColumNode = document.createElement('th')
            dataFirstColumNode.setAttribute('scope', 'row')
            dataFirstColumNode.innerText = currentData['id']
            dataRow.appendChild(dataFirstColumNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['studentId']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['name']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['surname']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['gpa']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['image'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)



        }
    })
}

async function loadOneStudent() {
    let studentId = document.getElementById('queryId').value
    if (studentId != '' && studentId != null) {
        let response = await fetch('https://dv-student-backend-2019.appspot.com/students/' + studentId)
        let data = await response.json()
        return data

    }

}

function createResultForm(data) {

    let resultElementForm = document.getElementById('resultForm')

    var checkwrapper = document.getElementById('wrapper')


    var wrapper = document.createElement('div')
    wrapper.setAttribute('id', 'wrapper')
    resultElementForm.appendChild(wrapper)

    if (checkwrapper != null) {
        resultElementForm.removeChild(checkwrapper)
    }

    data.then((json) => {


        let tital = document.createElement('h1')
        tital.innerHTML = "Profile"

        wrapper.appendChild(tital)

        var imageform = document.createElement('img')
        imageform.style.width = '550px'
        imageform.style.height = '450px'
        imageform.setAttribute('src', json['image'])


        let studenID = document.createElement('p')
        studenID.innerHTML = "Student ID : " + json['id']

        let fname = document.createElement('p')
        fname.innerHTML = "Name : " + json['name']

        let lname = document.createElement('p')
        lname.innerHTML = "Surname ID : " + json['surname']

        let gpa = document.createElement('p')
        gpa.innerHTML = "GPA : " + json['gpa']

        let penAmount = document.createElement('p')
        penAmount.innerHTML = "PenAmount : " + json['penAmount']

        let description = document.createElement('p')
        description.innerHTML = "Description : " + json['description']


        wrapper.appendChild(imageform)
        wrapper.appendChild(studenID)
        wrapper.appendChild(fname)
        wrapper.appendChild(lname)
        wrapper.appendChild(gpa)
        wrapper.appendChild(penAmount)
        wrapper.appendChild(description)


    })

}
